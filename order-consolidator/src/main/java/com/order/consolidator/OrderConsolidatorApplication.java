package com.order.consolidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Launch the Consolidator service on Spring-boot
 * @author Ismail
 *
 */
@SpringBootApplication
public class OrderConsolidatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderConsolidatorApplication.class, args);
	}

}
