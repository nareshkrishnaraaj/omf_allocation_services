package com.order.consolidator.services;

import java.util.List;

/**
 * Consolidates Order Line Status based on the rules defined by the implementing classes.
 * @author Ismail
 *
 * @param <T>
 */
public interface OrderLineStatusUpdate<T> {

	List<T> consolidate(List<T> t);
}
