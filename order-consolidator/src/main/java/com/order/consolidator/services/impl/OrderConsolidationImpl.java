package com.order.consolidator.services.impl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.ShipmentDetails.ShipmentDetail.Cartons.Carton;
import com.order.consolidator.exception.ServiceException;
import com.order.consolidator.services.OrderConsolidation;
import com.order.consolidator.services.OrderLineConsolidation;
import com.order.consolidator.services.OrderLineStatusUpdate;

/**
 * Service responsible for Consolidating the Split OrderLines. Post
 * Consolidation, remove duplicate orderlines and assigns the original
 * orderlineIds across the orderlines, DistributionOrderLines and
 * InvoiceDetails.
 * 
 * @author Ismail
 */
@Service
public final class OrderConsolidationImpl implements OrderConsolidation<TXML> {

	private static final Logger LOGGER = LogManager.getLogger(OrderConsolidationImpl.class.getName());

	@Autowired
	OrderLineConsolidation<OrderLine> orderLineConsolidationService;

	@Autowired
	AlignOrderLines orderLineAlignmentService;

	@Autowired
	OrderLineStatusUpdate<OrderLine> statusService;

	@Autowired
	InvoiceDetailConsolidation invoiceConsolidationService;
	
	@Autowired
	DistributionOrderConsolidation distributionConsolidation;
	
	@Autowired
	ShipmentDetailConsolidation shipmentConsolidation;
	
	@Autowired
	OrderLineStatusPatch orderlinepatch;

	@Override
	public TXML consolidate(TXML orderXml) {
		validateRequiredFields(orderXml);
		Map<String, String> mapping = getItemReferenceField15Mapping(orderXml);

		// Consolidates all the orderLines and their quantities, amounts based on the
		// ReferenceField15/
		// Eliminates the redundant orderlines post-consolidation.
		consolidateOrderLines(orderXml);
		orderLineAlignmentService.alignOrderLines(mapping, orderXml);
		consolidateInvoiceDetails(orderXml);
		consolidateDistributionDetails(orderXml);
		consolidateShipmentDetails(orderXml);
		return orderXml;
	}

	/**
	 * Pre-validates if all the required fields are present. If any of the required
	 * field is missing terminates the Consolidation process.
	 * 
	 * @param order
	 */
	private void validateRequiredFields(TXML order) {

		LOGGER.debug("Validating for the required fields");

		if (StringUtils.isEmpty(order) || StringUtils.isEmpty(order.getMessage())
				|| StringUtils.isEmpty(order.getMessage().getOrder()) || order.getMessage().getOrder().size() == 0)
			throw new ServiceException("Error while processing the order due to missing Order details");

		String orderNumber = !StringUtils.isEmpty(order.getMessage().getOrder().get(0).getOrderNumber())
				? order.getMessage().getOrder().get(0).getOrderNumber().getValue()
				: null;

		String storeOrderNumber = !StringUtils.isEmpty(order.getMessage().getOrder().get(0).getExternalOrderNumber())
				? order.getMessage().getOrder().get(0).getExternalOrderNumber().getValue()
				: null;

		if (StringUtils.isEmpty(orderNumber) && StringUtils.isEmpty(storeOrderNumber))
			throw new ServiceException("Error while processing the order due to missing Order number");

		LOGGER.info("Ecom Order# {}", orderNumber);
		LOGGER.info("XStore Order# {}", storeOrderNumber);

		if (StringUtils.isEmpty(order.getMessage().getOrder().get(0).getOrderLines())
				|| StringUtils.isEmpty(order.getMessage().getOrder().get(0).getOrderLines().getOrderLine())
				|| StringUtils.isEmpty(order.getMessage().getOrder().get(0).getOrderLines().getOrderLine().size())
				|| order.getMessage().getOrder().get(0).getOrderLines().getOrderLine().size() == 0)
			throw new ServiceException("Error while processing the order due to missing OrderLine details");

		// LineReferenceField15 is mandatory to tie the splitted items back.
		order.getMessage().getOrder().get(0).getOrderLines().getOrderLine().stream().forEach(o -> {
			if (StringUtils.isEmpty(o.getLineReferenceFields())
					|| StringUtils.isEmpty(o.getLineReferenceFields().getReferenceField15())
					|| StringUtils.isEmpty(o.getLineReferenceFields().getReferenceField15().getValue())) {
				throw new ServiceException("Error while processing the order due to missing ReferenceField15"
						+ " for item " + o.getLineNumber());
			}
		});
		LOGGER.info("Validated the RefField15 for all Lineitems");
	}

	
	/**
	 * Performs the consolidation of the OrderLines.
	 * @param txml
	 */
	private void consolidateOrderLines(TXML txml) {

		// group Orderlines by ReferenceField15
		Map<String, List<OrderLine>> groupedOrderLines = groupOrderLinesById(txml);

		// Updates the LineLevel status - Shipped/Released etc based on the Rules defined
		groupedOrderLines.entrySet().stream().forEach(entry -> statusService.consolidate(entry.getValue()));

		//Interim patch for AE-6316. Remove this block after DOM puts the actual fix.
		//start//
	//	LOGGER.info("Applying orderline status patch, if applicable...");
	//	groupedOrderLines.entrySet().stream().forEach(entry -> orderlinepatch.consolidate(entry.getValue(),txml));
		//end//
		
		// Performs the LineLevel Consolidation (first orderLine is retained, rest all are dropped after consolidating)
		List<OrderLine> consolidatedOrderLines = groupedOrderLines.entrySet().stream()
				.map(entry -> orderLineConsolidationService.consolidate(entry.getValue())).collect(Collectors.toList());

		LOGGER.info("Size of OrderLines after consolidation:::{}", consolidatedOrderLines.size());
		// clear all the OrderLines from the intiail Order xml before updating the
		// consolidated OrderLines
		txml.getMessage().getOrder().get(0).getOrderLines().getOrderLine().clear();
		txml.getMessage().getOrder().get(0).getOrderLines().getOrderLine().addAll(consolidatedOrderLines);
	}

	/**
	 * Performs the consolidation of InvoiceLineDetails
	 * 
	 * @param txml
	 */
	private void consolidateInvoiceDetails(TXML txml) {
		List<InvoiceDetail> invoiceDetails = txml.getMessage().getOrder().get(0).getInvoices().getInvoiceDetail();
		LOGGER.info("Invoking the Invoice Consolidation with size of invoiceDetails:{} ", invoiceDetails.size());
		invoiceConsolidationService.performConsolidation(invoiceDetails);
	}
	
	
	/**
	 * Performs the consolidation of DistributionDetails
	 * @param txml
	 */
	private void consolidateDistributionDetails(TXML txml) {
		List<DistributionOrder> distributionOrders = txml.getMessage().getOrder().get(0).getDistributionOrders().getDistributionOrder();
		LOGGER.info("Invoking the Distribution Consolidation with size of :{} ", distributionOrders.size());
		distributionConsolidation.performConsolidation(distributionOrders);
	}
	
	
	/**
	 * Consolidates the Shipment Details
	 * @param txml
	 */
	private void consolidateShipmentDetails(TXML txml) {
		if(ObjectUtils.isEmpty(txml.getMessage().getOrder().get(0).getShipmentDetails()) ||
				ObjectUtils.isEmpty(txml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail()) ||
				ObjectUtils.isEmpty(txml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getCartons()) ||
				txml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getTotalNumberOfCartons().compareTo(BigInteger.ZERO) == 0) return;
		
		List<Carton> cartons = txml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getCartons().getCarton();
		
		if(!ObjectUtils.isEmpty(cartons) && cartons.size() > 0) {
			LOGGER.info("Invoking the Shipment Consolidation with no. of cartons:{} ", cartons.size());
			cartons.stream().forEach(carton -> shipmentConsolidation.performConsolidation(carton.getCartonDetails().getCartonDetail()));
		}
	}

	
	/**
	 * Groups the OrderLines based on the ReferenceField15 i.e.,the actual
	 * orderline#
	 * 
	 * @param txml
	 * @return
	 */
	private Map<String, List<OrderLine>> groupOrderLinesById(TXML txml) {
		LOGGER.debug("Grouping OrderLines based on RefField15");
		Map<String, List<OrderLine>> groupedOrderLines = new HashMap<>();
		List<OrderLine> orderLines = txml.getMessage().getOrder().get(0).getOrderLines().getOrderLine();
		if (orderLines != null) {
			groupedOrderLines = orderLines.stream()
					.collect(Collectors.groupingBy(o -> o.getLineReferenceFields().getReferenceField15().getValue()));
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Grouping completed as:");
			groupedOrderLines.entrySet().forEach(s -> {
				LOGGER.debug("RefField15:{} --> OrderLine Count:{}", s.getKey(), s.getValue().size());
			});
		}
		return groupedOrderLines;
	}

	
	/**
	 * Generates a mapping of lineNumber and referenceField15.
	 * 
	 * @param txml
	 * @return
	 */
	private Map<String, String> getItemReferenceField15Mapping(TXML txml) {
		Map<String, String> mapping = new HashMap<String, String>();
		List<OrderLine> orderLines = txml.getMessage().getOrder().get(0).getOrderLines().getOrderLine();
		if (orderLines != null) {
			mapping = orderLines.stream().collect(Collectors.toMap(o -> o.getLineNumber(),
					o -> o.getLineReferenceFields().getReferenceField15().getValue()));
		}
		return mapping;
	}

}
