package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.PredicateManager;

/**
 * Consolidates the Line level amounts for the "LineTotal" for each OrderLine
 * 
 * @author Ismail
 *
 */

@Service
public class LineTotalConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(LineTotalConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();
		consolidateLineTotal(orderLines,firstOrderLine);
		consolidateQuantity(orderLines,firstOrderLine);
	}

	protected boolean validateRequiredFields(List<OrderLine> orderLines) {
		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (StringUtils.isEmpty(firstOrderLine.getLineTotal())
				|| StringUtils.isEmpty(firstOrderLine.getLineTotal().getValue()))
			return Boolean.FALSE;

		return Boolean.TRUE;
	}

	/**
	 * Consolidates the LineLevel Quantity : OrderedQuantity, CancelledQuantity, TotalCancelledQuantity,
	 * TotalAllocatedQuantity, AllocatedQuantity
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private void consolidateQuantity(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		//Aggregate the Total Ordered Quantity
		BigDecimal totalOrderedQuantity = orderLines.stream().map(o -> o.getQuantity().getOrderedQty())
				.reduce(BigDecimal.ZERO, (q1,q2) -> q1.add(q2));
		firstOrderLine.getQuantity().setOrderedQty(totalOrderedQuantity);

		//Aggregates the total Cancelled Quantity and Cancelled Quantity
		BigDecimal cancelledQuantity = 
				orderLines.stream().map(o -> o.getQuantity().getCancelledQty())
				.filter(PredicateManager.getNotNullFilter())
				.map(c -> c.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(c -> new BigDecimal(c))
				.reduce(BigDecimal.ZERO, (c1,c2) -> c1.add(c2));
		if(cancelledQuantity.intValue() >0)	{
			firstOrderLine.getQuantity().getCancelledQty().setValue(cancelledQuantity.toPlainString());
			firstOrderLine.getQuantity().getTotalCancelledQty().setValue(cancelledQuantity.toPlainString());
		}
		
		//Calculate the Allocated quantity, which is Ordered quantity minus Cancelled Qty.
		BigDecimal allocatedQuantity = totalOrderedQuantity.subtract(cancelledQuantity);
		firstOrderLine.getQuantity().getTotalAllocatedQty().setValue(allocatedQuantity.toPlainString());
	}

	/**
	 * Consolidates the OrderLine total
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private void consolidateLineTotal(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		BigDecimal lineTotal = orderLines.stream().map(o -> new BigDecimal(o.getLineTotal().getValue()))
				.reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2));
		firstOrderLine.getLineTotal().setValue(lineTotal.setScale(2, RoundingMode.FLOOR).toPlainString());
		LOGGER.info("LineTotal for item:::{} is {}", firstOrderLine.getItemID(),
				lineTotal.setScale(2, RoundingMode.FLOOR).toPlainString());
	}
}
