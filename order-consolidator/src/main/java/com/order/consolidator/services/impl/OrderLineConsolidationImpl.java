package com.order.consolidator.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.services.OrderLineConsolidation;

/**
 * Consolidates the provided OrderLines into a single OrderLine (the first orderline), including the
 * Linelevel amounts, adjustments, charges, taxes and other prices.
 * Post Consolidation returns the Consolidated Single OrderLine
 * 
 * 
 * @author Ismail
 */
@Service
public class OrderLineConsolidationImpl implements OrderLineConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(OrderLineConsolidationImpl.class);

	@Autowired
	LineTotalConsolidation itemtotalServie;

	@Autowired
	DiscountTotalConsolidation discountService;

	@Autowired
	ChargeTotalConsolidation chargeService;

	@Autowired
	TaxTotalConsolidation taxService;

	@Autowired
	ExtendedPriceTotalConsolidation extPriceService;

	@Autowired
	ExtendedPurchasePriceTotalConsolidation eppService;
	
	@Autowired
	AllocationDetailConsolidation allocationDetailsService;

	@Override
	public OrderLine consolidate(List<OrderLine> orderLines) {

		executeConsolidationServices(orderLines);
		dropAdditionalOrderLines(orderLines);

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		return optional.get();
	}

	/**
	 * Invokes the defined consolidation services for the given orderlines.
	 * 
	 * @param orderLines
	 */
	private void executeConsolidationServices(List<OrderLine> orderLines) {
		LOGGER.info("Invoking the consolidation services.");
		
		if(orderLines.size()<2) {
			return;
		}
		//Performs the consolidation and updates the first OrderLine
		List<AbstractBaseConsolidation> consolidatonServices = Arrays.asList(itemtotalServie, discountService,
				chargeService, taxService, extPriceService, eppService, allocationDetailsService);
		consolidatonServices.stream().forEach(c -> c.performConsolidation(orderLines));

		LOGGER.info("Completed the consolidation services");
	}

	/**
	 * Drops the other duplicate orderlines and updates the quantity accordingly
	 * 
	 * @param orderLines
	 */
	private void dropAdditionalOrderLines(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		if (!optional.isPresent())
			return;

		orderLines.removeIf(o -> !o.equals(optional.get()));
		LOGGER.info("Dropped additional orderLines and quantity updated.");
	}
}
