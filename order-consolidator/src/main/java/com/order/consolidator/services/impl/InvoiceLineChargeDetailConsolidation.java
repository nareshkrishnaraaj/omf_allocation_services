package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.ChargeDetails.ChargeDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.PredicateManager;

/**
 * Performs Consolidation of InvoiceLineDetails: ChargeDetails
 * 
 * @author Ismail
 *
 */
@Service
public class InvoiceLineChargeDetailConsolidation extends AbstractBaseConsolidation<InvoiceLineDetail> {

	private static final Logger LOGGER = LogManager.getLogger(InvoiceLineChargeDetailConsolidation.class);

	@Override
	public void performConsolidation(List<InvoiceLineDetail> invoiceLineDetails) {
		
		if (!validateRequiredFields(invoiceLineDetails))
			return;

		Optional<InvoiceLineDetail> optional = invoiceLineDetails.stream().findFirst();
		InvoiceLineDetail firstOrderLine = optional.get();

		// Fetch distinct charge categories on the first orderline
		Set<String> chargeIds = firstOrderLine.getChargeDetails().getChargeDetail().stream()
				.filter(PredicateManager.getNotNullFilter()).map(c -> c.getChargeCategory())
				.collect(Collectors.toSet());
		// Aggregate the charge amount towards each type and apply to the first
		// InvoiceLineDetail
		chargeIds.stream().forEach(chargeId -> {
			Predicate<ChargeDetail> predicate = c -> c.getChargeCategory().equals(chargeId);
			BigDecimal chargeAmount = getChargeAmount(invoiceLineDetails, predicate, chargeId);
			LOGGER.debug("{} Charge amount:::{}", chargeId, chargeAmount);
			applyCharge(firstOrderLine, chargeAmount, predicate, chargeId);
		});

		updateChargeDetails(invoiceLineDetails);
	}

	protected boolean validateRequiredFields(List<InvoiceLineDetail> invoiceLineDetails) {
		if (!super.validateRequiredFields(invoiceLineDetails))
			return Boolean.FALSE;

		Optional<InvoiceLineDetail> optional = invoiceLineDetails.stream().findFirst();
		InvoiceLineDetail firstinvoiceLineDetails = optional.get();

		if (StringUtils.isEmpty(firstinvoiceLineDetails.getChargeDetails())
				|| StringUtils.isEmpty(firstinvoiceLineDetails.getChargeDetails().getChargeDetail())
				|| firstinvoiceLineDetails.getChargeDetails().getChargeDetail().size() == 0)
			return Boolean.FALSE;
		return Boolean.TRUE;
	}

	private BigDecimal getChargeAmount(List<InvoiceLineDetail> orderLines, Predicate<ChargeDetail> predicate,
			Object chargeType) {
		return orderLines.stream().map(o -> o.getChargeDetails()).filter(Objects::nonNull)
				.flatMap(c -> c.getChargeDetail().stream()).filter(predicate).map(d -> d.getChargeAmount())
				.reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2));
	}

	private void applyCharge(InvoiceLineDetail orderLine, BigDecimal amount, Predicate<ChargeDetail> predicate,
			Object chargeType) {
		LOGGER.info("applyCharge for InvoiceDetailLine:::{} amount:::{} chargeType:::{}", orderLine.getInvoiceLineNbr(),
				amount, chargeType);
		orderLine.getChargeDetails().getChargeDetail().stream().filter(predicate).forEach(d -> {
			d.setChargeAmount(amount);
			d.getOriginalChargeAmount().setValue(amount.toPlainString());
		});
	}

	private void updateChargeDetails(List<InvoiceLineDetail> orderLines) {
		Optional<InvoiceLineDetail> optional = orderLines.stream().findFirst();
		InvoiceLineDetail firstOrderLine = optional.get();

		String refField15 = firstOrderLine.getParentOrderLineNbr();
		firstOrderLine.getChargeDetails().getChargeDetail().stream().forEach(t -> updateChargeDetailIds(t, refField15));
	}

	private void updateChargeDetailIds(ChargeDetail taxDetail, String refField15) {
		if (taxDetail != null && taxDetail.getExtChargeDetailId() != null) {
			int index = taxDetail.getExtChargeDetailId().indexOf("-");
			if (index != -1) {
				String prefix = taxDetail.getExtChargeDetailId().substring(0, index + 1);
				String newId = taxDetail.getExtChargeDetailId().replace(prefix, String.valueOf(refField15) + "-");
				taxDetail.setExtChargeDetailId(newId);
			}
		}
	}
}
