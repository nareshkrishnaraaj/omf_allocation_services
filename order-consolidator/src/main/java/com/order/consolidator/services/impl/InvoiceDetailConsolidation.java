package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.ChargeDetails.ChargeDetail;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.DiscountDetails;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.DiscountDetails.DiscountDetail;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.TaxDetails.TaxDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;

/**
 * Performs the consolidation of InvoiceDetails.
 * Based on the below format
 * <Invoices>
 * <InvoiceDetail>
 * <InvoiceNbr/>
 * <InvoiceLines>
 * <InvoiceLineDetail> ...</InvoiceLinedetail>
 * <InvoiceLineDetail> ...</InvoiceLinedetail>
 * </InvoiceDetail>
 * </InvoiceLines>
 * <InvoiceDetail>
 * <InvoiceNbr/>
 * <InvoiceLines>
 * <InvoiceLineDetail> ...</InvoiceLinedetail>
 * <InvoiceLineDetail> ...</InvoiceLinedetail>
 * </InvoiceLines>
 * </InvoiceDetail>
 * </Invoices>
 * 
 * 1. InvoiceDetails are grouped based on the InvoiceNbr & collection of InvoiceLines
 * 2. InvoiceLineDetails are grouped based on ParentOrderLineNbr & collection of InvoiceLineDetails
 * 3. Consolidation is performed on each set of InvoiceLineDetails having the same invoiceNbr and same parentOrderLineNbr.
 * @author Ismail
 *
 */

@Service
public class InvoiceDetailConsolidation extends AbstractBaseConsolidation<InvoiceDetail> {

	private static final Logger LOGGER = LogManager.getLogger(InvoiceDetailConsolidation.class);
	
	@Autowired
	InvoiceLineDetailConsolidation invoiceLineConsolidation;
	
	@Override
	public void performConsolidation(List<InvoiceDetail> invoiceDetails) {
		if(!validateRequiredFields(invoiceDetails))
			return;
		
		//Generate Key-value pair of InvoiceNbr of InvoiceDetail  and InvoiceLine
		Map<String, InvoiceLines> invoiceDetailMap = 
				invoiceDetails.stream().collect(Collectors.toMap(InvoiceDetail::getInvoiceNbr, InvoiceDetail::getInvoiceLines));
		
		//Iterate over each pair and invoke the consolidation of the InvoiceLineDetails for each of the InvoiceNbr.
		invoiceDetailMap.entrySet().stream().forEach(i -> {
			List<InvoiceLineDetail> invoiceLines = consolidateInvoices(i.getValue().getInvoiceLineDetail());
			i.getValue().getInvoiceLineDetail().clear(); //remove existing InvoiceLineDetails for the selected InvoiceNbr
			i.getValue().getInvoiceLineDetail().addAll(invoiceLines); //update the consolidated InvoiceLineDetails belonging to the selected InvoiceNbr
		});
		
		/**
		 * Systems are not built to handle multiple InvoiceDetails and expect only a single InvoiceDetail.
		 *  Hence, the below hack is to consolidate the InvoiceLineDetails across all the InvoiceDetails, grouping them based on the
		 *  parentOrderLine# and replicate the same details across all the InvoiceLineDetails for each parentOrderLineNbr.
		 *  There is no consolidation at InvoideDetails level.
		 *  When systems start supporting multiple invoiceDetails, below hack should be removed.
		 */
		//****** Starts Here *********
		LOGGER.info("Reconsolidating the InvoiceDetails");
		//	consolidateInvoiceLineDetailsAgain(invoiceDetails);
		
		//****** Ends Here *********
	}
	
	/**
	 * 
	 * @param invoiceLineDetails : All the InvoiceLineDetails passed must have the same invoiceNbr and part of the same <invoiceLine>. They may have different parentOrderLineNbr.
	 * @return : List of InvoiceLineDetails grouped based on the parentOrderLineNbr. Each parentOrderLineNbr will have only one invoiceLineDetail object.
	 */
	private List<InvoiceLineDetail> consolidateInvoices(List<InvoiceLineDetail> invoiceLineDetails) {
		//Group the invoiceLines based on the parentOrderLineNbr. All the invoiceLines in the collection have the same InvoiceNbr.
		//Following map should have Map {"parentOrderLineNbr" : List<InvoiceLineDetail>}, each value is a list of InvoiceLineDetail that can be aggregated as one.
		Map<String,List<InvoiceLineDetail>> parentOrderToInvoiceLineDetailsMap = groupInvoiceLinesByParentOrderLine(invoiceLineDetails);
		
		//Invoke consolidation of InvoiceLineDetails grouped by parentOrderLineNbr.
		List<InvoiceLineDetail> invoiceLines = 
				parentOrderToInvoiceLineDetailsMap.entrySet().stream().map(entry -> aggregateInvoiceLines(entry.getValue())).collect(Collectors.toList());
		return invoiceLines; //returning a list of Consolidated InvoiceLineDetails, each instance belonging to a different parentOrderLineNbr.
	}
	
	
	private InvoiceLineDetail aggregateInvoiceLines(List<InvoiceLineDetail> invoiceLines) {
		return invoiceLineConsolidation.consolidate(invoiceLines);
	}

	protected boolean validateRequiredFields(List<InvoiceDetail> invoiceDetails) {
		if (!super.validateRequiredFields(invoiceDetails))
			return Boolean.FALSE;

		Optional<InvoiceDetail> optional = invoiceDetails.stream().findFirst();
		InvoiceDetail firstInvoiceDetail = optional.get();
		if (firstInvoiceDetail.getInvoiceLines() != null
				&& firstInvoiceDetail.getInvoiceLines().getInvoiceLineDetail() != null)
			return Boolean.TRUE;
		
		return Boolean.FALSE;
	}
	
	/**
	 * Groups the InvoiceLineDetails based on the parentOderLineNbr
	 * @param invoiceLines
	 * @return
	 */
	private Map<String,List<InvoiceLineDetail>> groupInvoiceLinesByParentOrderLine(List<InvoiceLineDetail> invoiceLines) {
		Map<String,List<InvoiceLineDetail>> groupedIvoiceLines = new TreeMap<String,List<InvoiceLineDetail>>();
		groupedIvoiceLines = invoiceLines.stream().collect(Collectors.groupingBy(invoiceLine -> invoiceLine.getParentOrderLineNbr()));
		return groupedIvoiceLines;
	}
	
	/**
	 * Read the comment provided where this method is invokced. Not to be used anywhere else.
	 * @param invoiceDetails
	 */
	private void consolidateInvoiceLineDetailsAgain(List<InvoiceDetail> invoiceDetails) {
		if(!Objects.isNull(invoiceDetails)) {
			
			//Fetch all the InvoiceLineDetails, across all invoiceDetails except for the InvoiceType "return"
			List<InvoiceLineDetail> invoiceLineDetails = 
					invoiceDetails.stream()
					.filter(invoiceDetail -> !invoiceDetail.getInvoiceType().equalsIgnoreCase("return"))
					.flatMap(invoiceDetail -> invoiceDetail.getInvoiceLines().getInvoiceLineDetail().stream())
					.collect(Collectors.toList());
			
			//Group them based on the parentOrderLineNbr
			Map<String, List<InvoiceLineDetail>> groupedInvoiceLineDetails = 
			invoiceLineDetails.stream()
			.collect(Collectors.groupingBy(invoiceLineDetail -> invoiceLineDetail.getParentOrderLineNbr()));
			
			//Aggregate the Monetory values for all the InvoiceLineDetails as grouped by parentOrderLineNbr
			//and the aggregated values will reflect across all the InvoiceLineDetails participated in aggregation.
			
			groupedInvoiceLineDetails.entrySet().stream().forEach(entry -> {
				addHeaderValues(entry.getValue());
				consolidateChargeDetails(entry.getValue());
				consolidateTaxDetails(entry.getValue());
				consolidateDisocuntDetails(entry.getValue());
			});
			LOGGER.info("Consolidated the Invoices");
		}
	}
	
	/**
	 * Consoldiate the base values for InvoiceLineDetails
	 * @param invoiceLineDetails
	 */
	private void addHeaderValues(List<InvoiceLineDetail> invoiceLineDetails) {
		//All the invoiceLineDetails belong to the same parentOrderLineNbr
		//Consolidate the values and assign against all the InvoiceLineDetails
		
		BigDecimal orderedQuantity = 
				invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getOrderedQty()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal invoicedQuantity = 
				invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getInvoicedQuantity()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal shippedQuantity = 
				invoiceLineDetails.stream()
				.filter(invoiceLine -> !Objects.isNull(invoiceLine.getShippedQuantity()) 
						&& !StringUtils.isEmpty(invoiceLine.getShippedQuantity().getValue()))
				.map(invoiceLine -> new BigDecimal(invoiceLine.getShippedQuantity().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal lineCharges = 
				invoiceLineDetails.stream()
				.filter(invoiceLine -> !Objects.isNull(invoiceLine.getLineCharges()) 
						&& !StringUtils.isEmpty(invoiceLine.getLineCharges().getValue()))
				.map(invoiceLine -> new BigDecimal(invoiceLine.getLineCharges().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal lineTaxes = 
				invoiceLineDetails.stream()
				.filter(invoiceLine -> !Objects.isNull(invoiceLine.getLineTax()) 
						&& !StringUtils.isEmpty(invoiceLine.getLineTax().getValue()))
				.map(invoiceLine -> new BigDecimal(invoiceLine.getLineTax().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal lineDiscounts = 
				invoiceLineDetails.stream()
				.filter(invoiceLine -> !Objects.isNull(invoiceLine.getLineDiscounts()) 
						&& !StringUtils.isEmpty(invoiceLine.getLineDiscounts().getValue()))
				.map(invoiceLine -> new BigDecimal(invoiceLine.getLineDiscounts().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal lineTotal = 
				invoiceLineDetails.stream()
				.filter(invoiceLine -> !Objects.isNull(invoiceLine.getLineTotal()) 
						&& !StringUtils.isEmpty(invoiceLine.getLineTotal().getValue()))
				.map(invoiceLine -> new BigDecimal(invoiceLine.getLineTotal().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal lineSubtotal = 
				invoiceLineDetails.stream()
				.filter(invoiceLine -> !Objects.isNull(invoiceLine.getLineSubTotal()) 
						&& !StringUtils.isEmpty(invoiceLine.getLineSubTotal().getValue()))
				.map(invoiceLine -> new BigDecimal(invoiceLine.getLineSubTotal().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		invoiceLineDetails.stream().forEach(invoiceLineDetail -> {
			invoiceLineDetail.setOrderedQty(orderedQuantity);
			invoiceLineDetail.setInvoicedQuantity(invoicedQuantity);
			invoiceLineDetail.getShippedQuantity().setValue(shippedQuantity.toPlainString());
			invoiceLineDetail.getLineCharges().setValue(lineCharges.toPlainString());
			invoiceLineDetail.getLineTax().setValue(lineTaxes.toPlainString());
			invoiceLineDetail.getLineDiscounts().setValue(lineDiscounts.toPlainString());
			invoiceLineDetail.getLineTotal().setValue(lineTotal.toPlainString());
			invoiceLineDetail.getLineSubTotal().setValue(lineSubtotal.toPlainString());
		});
	}
	
	/**
	 * 
	 * @param invoiceLineDetails
	 */
	private void consolidateChargeDetails(List<InvoiceLineDetail> invoiceLineDetails) {
		//All the invoiceLineDetails belong to the same parentOrderLineNbr
		//Get the ChargeDetails for all the InvoiceLineDetails
		List<ChargeDetail> chargeDetails =	
				invoiceLineDetails.stream()
				.flatMap(invoiceLineDetail -> invoiceLineDetail.getChargeDetails().getChargeDetail().stream()).collect(Collectors.toList());
		
		//Group the above InvoiceLineDetails based on the combination of chargeDetailsId~chargeCategory and consolidate those values.
		Map<String,List<ChargeDetail>> chargeGrouping =
		chargeDetails.stream().collect(Collectors.groupingBy(chargeDetail -> chargeDetail.getExtChargeDetailId() +"~"+chargeDetail.getChargeCategory()));
		
		//add and update the chargedetails
		chargeGrouping.entrySet().stream().forEach(entry -> addApplyChargeDetails(entry.getValue()));
	}
	
	
	/**
	 * Consolidate the chargeDetail based on the Charge type across all the InvoiceLineDetails and invoiceDetails.
	 * @param chargeDetails
	 */
	private void addApplyChargeDetails(List<ChargeDetail> chargeDetails) {
		
		//Consolidate the Dollar value entities
		BigDecimal chargeAmount = 
				chargeDetails.stream().map(chargeDetai -> chargeDetai.getChargeAmount()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal unitCharge = 
				chargeDetails.stream()
				.filter(chargeDetail -> !Objects.isNull(chargeDetail.getUnitCharge()))
				.filter(chargeDetail -> !ObjectUtils.isEmpty(chargeDetail.getUnitCharge().getValue()))
				.map(chargeDetail -> new BigDecimal(chargeDetail.getUnitCharge().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal originalChargeAmount = 
				chargeDetails.stream()
				.filter(chargeDetail -> !Objects.isNull(chargeDetail.getOriginalChargeAmount()))
				.filter(chargeDetail -> !ObjectUtils.isEmpty(chargeDetail.getOriginalChargeAmount().getValue()))
				.map(chargeDetail -> new BigDecimal(chargeDetail.getOriginalChargeAmount().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		chargeDetails.stream().forEach(chargeDetail -> {
			chargeDetail.setChargeAmount(chargeAmount);
			chargeDetail.getUnitCharge().setValue(unitCharge.toPlainString());
			chargeDetail.getOriginalChargeAmount().setValue(originalChargeAmount.toPlainString());
		});
	}
	
	/**
	 * Consolidate TaxDetails across all the invoice linedetail, grouped by extTaxDetailId~requestType
	 * @param invoiceLineDetails
	 */
	private void consolidateTaxDetails(List<InvoiceLineDetail> invoiceLineDetails) {
		//All the invoiceLineDetails belong to the same parentOrderLineNbr
		//Get the TaxDetails for all the InvoiceLineDetails
		List<TaxDetail> taxDetails = 
				invoiceLineDetails.stream()
				.flatMap(invoiceLineDetail -> invoiceLineDetail.getTaxDetails().getTaxDetail().stream())
				.collect(Collectors.toList());
		
		boolean isTaxDetailIdsWithDash =taxDetails.stream().map(taxDetail -> taxDetail.getExtTaxDetailId()).allMatch(id -> id.indexOf("-")>0);
		if(isTaxDetailIdsWithDash) {
			Map<String,List<TaxDetail>> taxDetailGrouping = 
					taxDetails.stream()
					.collect(Collectors.groupingBy(taxDetail -> taxDetail.getExtTaxDetailId()+"~"+taxDetail.getChargeCategory().getValue()));
			
			//add and update the TaxDetails
			taxDetailGrouping.entrySet().stream().forEach(entry -> addApplyTaxDetails(entry.getValue()));
		} else {
			//Group the TaxDetails grouping them by ExtTaxDetailId
			Map<String,List<TaxDetail>> taxDetailGrouping = 
					taxDetails.stream()
					.collect(Collectors.groupingBy(taxDetail -> taxDetail.getTaxCategory()+"~"+taxDetail.getChargeCategory().getValue()));
			
			//add and update the TaxDetails
			taxDetailGrouping.entrySet().stream().forEach(entry -> addApplyTaxDetails(entry.getValue()));
		}
	}
	
	/**
	 * Adds up the TaxDetail monteroy values and apply towards all the TaxDetails.
	 * @param taxDetails
	 */
	private void addApplyTaxDetails(List<TaxDetail> taxDetails) {
		//TaxAmount
		BigDecimal taxAmount = taxDetails.stream().map(taxDetail -> taxDetail.getTaxAmount()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		//TaxableAmount
		BigDecimal taxableAmount = taxDetails.stream()
		.filter(taxDetail -> !Objects.isNull(taxDetail.getTaxableAmount()))
		.filter(taxDetail -> !StringUtils.isEmpty(taxDetail.getTaxableAmount().getValue()))
		.map(taxDetail -> new BigDecimal(taxDetail.getTaxableAmount().getValue()))
		.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		taxDetails.stream().forEach(taxDetail -> {
			taxDetail.setTaxAmount(taxAmount);
			taxDetail.getTaxableAmount().setValue(taxableAmount.toPlainString());
		});
		
	}
	
	private void consolidateDisocuntDetails(List<InvoiceLineDetail> invoiceLineDetails) {
		//Get all the discountDetails
		List<DiscountDetail> discountDetails =
		invoiceLineDetails.stream().flatMap(invoiceDetail -> invoiceDetail.getDiscountDetails().getDiscountDetail().stream()).collect(Collectors.toList());
		
		//Group discountDetails based ExtDiscountId~DiscountType
		Map<String, List<DiscountDetail>> groupedDiscounts = 
		discountDetails.stream()
		.collect(Collectors.groupingBy(discountDetail -> discountDetail.getExtDiscountId()+"~"+discountDetail.getDiscountType()));
		
		//add and update the discountdetails
		groupedDiscounts.entrySet().stream().forEach(entry -> addApplyDisocuntDetails(entry.getValue()));
	}
	
	/**
	 * Adds up the DiscountDetail monetory values and apply towards all the DiscountDetails.
	 * @param discountDetails
	 */
	private void addApplyDisocuntDetails(List<DiscountDetail> discountDetails) {
		//DiscountAmount
		BigDecimal discountAmount = 
				discountDetails.stream().map(discountDetail -> discountDetail.getDiscountAmount())
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal discountValue = 
				discountDetails.stream()
				.filter(discountDetail -> !Objects.isNull(discountDetail.getDiscountValue()))
				.filter(discountDetail -> !StringUtils.isEmpty(discountDetail.getDiscountValue().getValue()))
				.map(discountDetail -> new BigDecimal(discountDetail.getDiscountValue().getValue()))
				.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		discountDetails.stream().forEach(discountDetail -> {
			discountDetail.setDiscountAmount(discountAmount);
			discountDetail.getDiscountValue().setValue(discountValue.toPlainString());
		});
	}
}
