package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine.DiscountDetails.DiscountDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.Constants;
import com.order.consolidator.util.PredicateManager;

/**
 * Consolidates the Discount amounts of each type for each of the OrderLine. The
 * discount types considered are Coupon, Employee, BRD and Promotion. Promotion
 * Discount has different types underneath, each of them is identified based on
 * the identifiers deifned below.
 * 
 * @author Ismail
 *
 */

@Service
public class DiscountTotalConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(DiscountTotalConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;

		/**
		 * Since all the orderLines in the list are of same type with quantity=1, 1.
		 * Identify the unique discount types from the first orderline 2. Aggregate the
		 * discount totals by the discount type 3. Apply the aggregated discount total
		 * towards each discount type to the first orderline.
		 */
		consolidateCouponDiscounts(orderLines);
		consolidateEmployeeDiscounts(orderLines);
		consolidateBRDDiscounts(orderLines);
		consolidatePromotionDiscounts(orderLines);

		updateDiscountDetailLineIds(orderLines);
	}

	protected boolean validateRequiredFields(List<OrderLine> orderLines) {
		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (StringUtils.isEmpty(firstOrderLine.getDiscountDetails())
				|| StringUtils.isEmpty(firstOrderLine.getDiscountDetails().getDiscountDetail())
				|| firstOrderLine.getDiscountDetails().getDiscountDetail().size() == 0)
			return Boolean.FALSE;

		return Boolean.TRUE;
	}

	/**
	 * Consolidates the Coupon discounts and applies towards the first OrderLine
	 * 
	 * @param orderLines
	 */
	private void consolidateCouponDiscounts(List<OrderLine> orderLines) {

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		Set<String> coupons = firstOrderLine.getDiscountDetails().getDiscountDetail().stream()
				.filter(PredicateManager.getCouponFilter().and(PredicateManager.getNotNullFilter()))
				.map(d -> d.getCouponId().getValue()).collect(Collectors.toSet());

		/* addEmployeeDiscounts(); addBRDDiscounts(); addPromotionDiscounts(); */

		coupons.stream().forEach(c -> {
			Predicate<DiscountDetail> predicate = PredicateManager.getCouponFilter(c);
			BigDecimal discountAmount = getDiscount(orderLines, predicate, c);
			applyDiscount(firstOrderLine, discountAmount, predicate, c);
		});
	}

	/**
	 * Consolidates the Employee discounts and applies towards the first OrderLine
	 * Employee discount has two variations. One with 20% and another with 30%.
	 * There can also be multiple Employee discounts on the same lineitem with any of the above two combinations.
	 * Discount percentage is the only key to differentiate b/w the two discounts.
	 * In some-cases there is no discount percentage to differentiate b/w the types of Employee discount.
	 * @param orderLines
	 * @param percentage
	 * @return
	 */
	private void consolidateEmployeeDiscounts(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		List<BigDecimal> empDiscountIds = firstOrderLine.getDiscountDetails().getDiscountDetail().stream()
				.filter(PredicateManager.getEmployeeDiscountFilter()
						.and(PredicateManager.getNotNullFilter()))
				.map(d -> d.getDiscountPercentage() != null ? d.getDiscountPercentage().getValue() : BigDecimal.ZERO)
				.collect(Collectors.toList());

		if(empDiscountIds.size()>1) {
			//Group the discounts based on the external dicountId  like 1-1, 1-2,1-3,1-4 and 2-1,2-2,2-3,2-4 all should be grouped as (1-1,2-1) , (
			Map<String,List<DiscountDetail>> discountMap = 
			orderLines.stream().flatMap(orderLine -> orderLine.getDiscountDetails().getDiscountDetail().stream())
				.filter(PredicateManager.getEmployeeDiscountFilter()
						.and(PredicateManager.getNotNullFilter()))
				.collect(Collectors.groupingBy(discount ->discount.getExtDiscountId().split("-")[1]));
			
			discountMap.entrySet().stream().forEach(entryMap -> addEmployeeDiscounts(entryMap.getValue(),entryMap.getKey(),firstOrderLine));
		} else {
			empDiscountIds.stream().forEach(discountId -> {
				Predicate<DiscountDetail> predicate = PredicateManager.getEmployeeDiscountFilterById(discountId);
				BigDecimal discountAmount = getDiscount(orderLines, predicate, discountId);
				applyDiscount(firstOrderLine, discountAmount, predicate, discountId);
			});
		}
	}
	
	/**
	 * Updates the DiscountDetails used specifically for the Employee Discount types of more than 1 on each lineitem.
	 * @param discountDetails
	 * @param extDiscountId
	 * @param firstOrderLine
	 */
	private void addEmployeeDiscounts(List<DiscountDetail> discountDetails, String extDiscountId, OrderLine firstOrderLine) {
		BigDecimal discountAmount = 
				discountDetails.stream()
		.filter(PredicateManager.getEmployeeDiscountFilter())
		.map(discountDetail -> discountDetail.getDiscountAmount()).reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		
		
		//Update the DiscountDetail of the FirstOrderLine having the ExtDiscountId same as the key (the disocuntId used to group the employee Discounts)
		firstOrderLine.getDiscountDetails().getDiscountDetail()
		.stream().
		filter(PredicateManager.getEmployeeDiscountFilter()
				.and(x -> x.getExtDiscountId().contains("-"))
				.and(x -> (x.getExtDiscountId().split("-")[1]).equals(extDiscountId)))
		.forEach(discountDetail -> {
			discountDetail.setDiscountAmount(discountAmount);
			discountDetail.getDiscountValue().setValue(discountAmount.toPlainString());
		});
		
	}

	
	/**
	 * Consolidates BRD Discounts and applies towards the first OrderLine
	 * 
	 * @param orderLines
	 */
	private void consolidateBRDDiscounts(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		// If multiple discount percentages are present, discounts are grouped against
		// each percentage.
		Set<String> brdDiscountIds = firstOrderLine.getDiscountDetails().getDiscountDetail().stream()
				.filter(PredicateManager.getBRDFilter().and(PredicateManager.getNotNullFilter()))
				.map(d -> d.getCouponId().getValue()).collect(Collectors.toSet());

		brdDiscountIds.stream().forEach(discountId -> {
			Predicate<DiscountDetail> predicate = PredicateManager.getBRDFilterById(discountId);
			BigDecimal discountAmount = getDiscount(orderLines, predicate, discountId);
			applyDiscount(firstOrderLine, discountAmount, predicate, discountId);
		});
	}

	/**
	 * Consolidates promotion discounts of type - Shipping, Handling, GiftBox (each
	 * of these have a unique ExternalDiscountId) which is used as disocuntId. For
	 * any other type, discountId is discount percentage if present. Else,
	 * description is discountId.
	 * 
	 * @param orderLines
	 */
	private void consolidatePromotionDiscounts(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		Set<String> promoDiscountIds = firstOrderLine.getDiscountDetails().getDiscountDetail().stream()
				.filter(PredicateManager.getPromotionFilter().and(PredicateManager.getNotNullFilter()))
				.map(d -> extractDiscountId(d)).collect(Collectors.toSet());

		Predicate<DiscountDetail> promotionPredicate = PredicateManager.getPromotionFilter();

		promoDiscountIds.stream().forEach(discountId -> {

			Predicate<DiscountDetail> promotionPredicateById = PredicateManager
					.getPromotionFilterByDiscId(discountId);
			Predicate<DiscountDetail> promotionPredicateByDiscPc = PredicateManager
					.getPromotionFilterByDiscPercent(discountId);
			Predicate<DiscountDetail> promotionPredicateByDiscDesc = PredicateManager
					.getPromotionFilterByDesc(discountId);
			Predicate<DiscountDetail> criteria = promotionPredicate
					.and(promotionPredicateById.or(promotionPredicateByDiscPc).or(promotionPredicateByDiscDesc));
			BigDecimal discountAmount = getDiscount(orderLines, criteria, discountId);
			applyDiscount(firstOrderLine, discountAmount, criteria, discountId);
		});
	}

	/**
	 * Fetches the aggregated discount amount for the provided discount type filter
	 * and discount type key
	 * 
	 * @param orderLines
	 * @param predicate
	 * @param discountTypeKey
	 * @return
	 */
	private BigDecimal getDiscount(List<OrderLine> orderLines, Predicate<DiscountDetail> predicate,
			Object discountTypeKey) {
		return orderLines.stream().map(o -> o.getDiscountDetails()).filter(Objects::nonNull)
				.flatMap(d -> d.getDiscountDetail().stream()).filter(predicate)
				.map(d -> d.getDiscountAmount())
				.reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2));
	}

	/**
	 * Applies the provided discount amount against the discount type and
	 * discounttype key provided.
	 * 
	 * @param orderLine
	 * @param amount
	 * @param predicate
	 * @param discountTypeKey
	 */
	private void applyDiscount(OrderLine orderLine, BigDecimal amount, Predicate<DiscountDetail> predicate,
			Object discountTypeKey) {
		LOGGER.info("applyDiscount for orderLine:::{} amount:::{} discountTypeKey:::{}", orderLine.getItemID(), amount,
				discountTypeKey);

		orderLine.getDiscountDetails().getDiscountDetail().stream().filter(predicate).forEach(d -> {
			d.setDiscountAmount(amount);
			if(!StringUtils.isEmpty(d.getDiscountValue()))	d.getDiscountValue().setValue(amount.toPlainString());
		});
	}

	/**
	 * Extracts the discount Id based on the available parameters in the order of
	 * preference as : ExternalDiscountId, DiscountPercentage (!Null, !Empty, !0) , DiscountDescription.
	 * 
	 * @param discDetails
	 * @return
	 */
	private String extractDiscountId(DiscountDetail discDetails) {

		String discountId = Constants.BLANK;
		if (discDetails != null) {
			discountId = (discDetails.getExtDiscountId() != null && discDetails.getExtDiscountId().indexOf("-") == -1)
					? discDetails.getExtDiscountId()
					: (isNonZeroPercentDiscount(discDetails)
									? (discDetails.getDiscountPercentage().getValue().toPlainString())
									: ((discDetails.getDescription() != null
											&& discDetails.getDescription().getValue() != null)
													? (discDetails.getDescription().getValue())
													: (Constants.BLANK)));
		}
		return discountId;
	}

	/**
	 * Updates the DiscountDetail - linenumbers with the original LineNbrs for the
	 * first orderline
	 * 
	 * @param orderLines
	 */
	private void updateDiscountDetailLineIds(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		String refField15 = firstOrderLine.getLineReferenceFields().getReferenceField15().getValue();
		List<DiscountDetail> discountDetails = firstOrderLine.getDiscountDetails().getDiscountDetail();
		discountDetails.stream().forEach(d -> updateDiscountIds(d, refField15));
	}

	/**
	 * Updates the DiscountDetail - linenumbers with the provided linenumber
	 * 
	 * @param discountDetail
	 * @param refField15
	 */
	private void updateDiscountIds(DiscountDetail discountDetail, String refField15) {
		if (discountDetail.getExtDiscountDetailId() != null) {
			int index = discountDetail.getExtDiscountDetailId().indexOf(Constants.HYPHEN);
			if (index != -1) {
				String prefix = discountDetail.getExtDiscountDetailId().substring(0, index + 1);
				if (prefix != null && prefix.length() > 0) {
					String newId = discountDetail.getExtDiscountDetailId().replace(prefix,
							String.valueOf(refField15) + Constants.HYPHEN);
					discountDetail.setExtDiscountDetailId(newId);
				}
			}
		}
		if (discountDetail.getExtDiscountId() != null) {
			int index = discountDetail.getExtDiscountId().indexOf(Constants.HYPHEN);
			if (index != -1) {
				String prefix = discountDetail.getExtDiscountId().substring(0, index + 1);
				if (prefix != null && prefix.length() > 0) {
					String newId = discountDetail.getExtDiscountId().replace(prefix,
							String.valueOf(refField15) + Constants.HYPHEN);
					discountDetail.setExtDiscountId(newId);
				}
			}
		}
	}
	
	/**
	 * If the discount percentage is the discountId, then it needs to be a Positive numeric value.
	 * @param discDetails
	 * @return
	 */
	private boolean isNonZeroPercentDiscount(DiscountDetail discDetails) {
		if(discDetails == null || 
				discDetails.getDiscountPercentage() == null || 
				discDetails.getDiscountPercentage().getValue() == null)	return Boolean.FALSE;
		
		BigDecimal discountPercentValue = discDetails.getDiscountPercentage().getValue();
		
		if(discountPercentValue.compareTo(BigDecimal.ZERO)==0) return Boolean.FALSE;
		
		return Boolean.TRUE;
	}
}
