package com.order.consolidator.util;

import java.math.BigDecimal;

public interface Constants {
		
	BigDecimal ZERO_VALUE = new BigDecimal(0);
	BigDecimal ONE_VALUE = new BigDecimal("1.0");
	String HYPHEN = "-";
	String COUPON = "Coupon";
	String PROMOTION = "Promotion";
	String EMPLOYEE = "Employee";
	String BRD = "BRD";
	String APPEASEMENT = "Appeasement";
	String BLANK = "";
	
	//Define the error Messages
	String BAD_REQUEST_ERROR = "Bad Request";
	String REQUEST_UNAUTHORIZED = "Unauthorized Request";
	String INTERNAL_ERROR = "Service Error";
}
