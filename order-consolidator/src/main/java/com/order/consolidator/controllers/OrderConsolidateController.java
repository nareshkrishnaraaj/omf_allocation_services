package com.order.consolidator.controllers;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.order.consolidator.entities.TXML;
import com.order.consolidator.exception.ServiceException;
import com.order.consolidator.services.OrderConsolidation;
import com.order.consolidator.util.XMLUtil;

/**
 * Controller to handle the REST calls for the Consolidator app.
 * @author Ismail
 *
 */
@RestController
public class OrderConsolidateController {

	@Autowired
	OrderConsolidation<TXML> consolidatorService;

	@Autowired
	XMLUtil util;

	private static final Logger LOGGER = LogManager.getLogger(OrderConsolidateController.class);

	@RequestMapping(path = "/v1/order/consolidate", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> mergeOrder(@RequestBody String txml, @RequestHeader("ApiKey") String apiKey) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		ThreadContext.push(UUID.randomUUID().toString());
		LOGGER.info("*******************Initiating consolidation new request*******************");
		TXML responseXml;
		try {
			responseXml = consolidatorService.consolidate(util.unmarshallRequest(txml));
			String responseBody = util.marshallRequest(responseXml);
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getStackTrace());
			e.printStackTrace();
			throw new ServiceException(e);
		} finally {
			stopWatch.stop();
			LOGGER.info("*******************Request completed in {} ms *******************", stopWatch.getLastTaskTimeMillis());
			ThreadContext.pop();
		}
	}

}
