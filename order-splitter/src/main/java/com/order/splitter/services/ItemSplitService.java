package com.order.splitter.services;

import java.util.List;

public interface ItemSplitService<T> {
	
	List<T> splitItem(T t);

}
