package com.order.splitter.services;



public interface OrderSplitService<T> {

	public T splitOrder(T t);
}
