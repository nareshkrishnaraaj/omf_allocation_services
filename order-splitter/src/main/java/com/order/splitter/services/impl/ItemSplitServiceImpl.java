package com.order.splitter.services.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.splitter.engine.ProrationEngine;
import com.order.splitter.entities.ObjectFactory;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.services.ItemCloneService;
import com.order.splitter.services.ItemSplitService;

/**
 * Responsible for splitting a multi-quantity orderline to multi-orderline of quantity 1 each.
 * @author Ismail
 *
 */
@Service
public class ItemSplitServiceImpl implements ItemSplitService<OrderLine> {
	
	private static final Logger LOGGER = LogManager.getLogger(ItemSplitServiceImpl.class.getName());
	
	@Autowired
	ItemCloneService<OrderLine> cloneService;
	
	@Autowired
	ProrationEngine engine;

	@Override
	public List<OrderLine> splitItem(OrderLine orderLine) {
		LOGGER.info("Invoking Item Split service");
		return splitOrderLine(orderLine);
	}

	/**
	 * The fields that should to be split include - LineTotal, Shipping>ChargeAmount, Handling>ChargeAmount,
	 * DiscountDetail>DiscountAmount, TaxDetails (Sales, shipping, handling) >TaxAmount, CustomField > ExtendedPurchasePrice
	 * @param orderLine
	 * @return
	 */
	private List<OrderLine> splitOrderLine(OrderLine orderLine){
		updateItemMappingKey(orderLine);
		Float qty = orderLine.getQuantity().getOrderedQty().floatValue();
		List<OrderLine> orderLines = cloneService.clone(orderLine, qty.intValue()-1);
		orderLines.add(orderLine);
		LOGGER.info("No. of copies of OrderLine::: {}",orderLines.size());
		return prorateOrderLines(orderLines);
	}
	
	private List<OrderLine> prorateOrderLines(List<OrderLine> orderLines) {
		LOGGER.info("*******Invoking proration engine*******");
		List<OrderLine> adjustedOrderLines = engine.executeProration(orderLines);
		LOGGER.info("**********Proration complete**********");
		return adjustedOrderLines;
	}
	
	private void updateItemMappingKey(OrderLine orderLine) {
		if(orderLine != null && orderLine.getLineReferenceFields() != null ) {
			String lineId = orderLine.getLineNumber();
			if(orderLine.getLineReferenceFields().getReferenceField15() == null) {
				orderLine.getLineReferenceFields().setReferenceField15(
						new ObjectFactory().createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField15(lineId));
				LOGGER.debug("created new ReferenceField15 for line# {}",lineId);
				
			} else {
				orderLine.getLineReferenceFields().getReferenceField15().setValue(lineId);
				LOGGER.debug("Updated ReferenceField15 with line# {}",lineId);
			}
		}
	}
}
