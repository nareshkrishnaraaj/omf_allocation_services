package com.order.splitter.auth;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import com.order.splitter.exception.*;
/**
 * Class responsible for Client Authorization
 * @author Ismail
 *
 */
@Aspect
@Configuration
public class ClientAuthValidationService {
	
	@Value("${clientid}")
	private String clientId;
	
	@Value("${password}")
	private String password;
	
	@Before("execution(* com.order.splitter.controllers.*.*(..))")
	public void authorizeClient(JoinPoint joinPoint) throws ServiceException{
		Object[] args = joinPoint.getArgs();
		String apiKey = null;
		boolean isAuthorized = false;
		if(args.length>1) {
			apiKey = String.valueOf(args[1]);
			isAuthorized = isClientAuthorized(apiKey);
		}
		if(!isAuthorized) {
			ServiceException se = new ServiceException("Client not authorized to access this service");
			ServiceError error = new ServiceError();
			error.setErrorCode(HttpStatus.UNAUTHORIZED);
			error.setErrorDescription("Client not authorized to access this service");
			error.setErrorMessages(Arrays.asList("Not authorized"));
			se.setErrorMessages(error);
			throw se;
		}
	}
	
	/**
	 * Authorizes the ApiKey from the header.
	 * The expected format of the ApiKey is ClientId|HttpMethod|URI|Password
	 * @param apiKey
	 * @return
	 */
	private boolean isClientAuthorized(String apiKey) {
		if(StringUtils.isEmpty(apiKey))
			return false;
		
		byte[] decodedBytes = Base64Utils.decodeFromString(apiKey);
		String decodedString = new String(decodedBytes);
		String[] splitKey = decodedString.split("\\|");
		if(splitKey.length != 4)
			return false;
		
		if(splitKey[0].equals(clientId) && splitKey[1].equals("POST") && 
				splitKey[2].equals("v1/order/split") && splitKey[3].equals(password))
			return true;
		
		return false;
	}

}
