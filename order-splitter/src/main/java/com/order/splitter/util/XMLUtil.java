package com.order.splitter.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXParseException;
import com.order.splitter.entities.TXML;
import com.order.splitter.exception.ServiceException;

/**
 * Utility class for Object Serialization to XML and XML De-serialization
 * 
 * @author Ismail
 *
 */
@Service
public class XMLUtil {

	private static final Logger LOGGER = LogManager.getLogger(XMLUtil.class);

	/**
	 * Deserializes the given XML String to the Object
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TXML unmarshallRequest(String request) throws Exception {
		LOGGER.info("Initiating the unmarshallRequest");
		LOGGER.debug("Request:: {}", request);
		try {
			JAXBContext jaxbContext = JAXBContextLoader.CACHED_CONTEXT;
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			TXML xml = (TXML) unmarshaller.unmarshal(new StringReader(request));
			return xml;
		} catch (JAXBException e) {
			LOGGER.error(e.getMessage(), e);
			if (e.getLinkedException() instanceof SAXParseException)
				throw (SAXParseException) e.getLinkedException();
			throw e;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Serializes the Object to the XML String
	 * 
	 * @param xml
	 * @return
	 * @throws JAXBException
	 */
	public String marshallRequest(TXML xml) throws JAXBException {
		LOGGER.info("Initiating the marshallRequest");
		try {
			JAXBContext jaxbContext = JAXBContextLoader.CACHED_CONTEXT;
			Marshaller marshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();
			marshaller.marshal(xml, writer);
			LOGGER.debug("Response:: {}", writer.toString());
			return writer.toString();
		} catch (JAXBException e) {
			LOGGER.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * One time initialization of the JAXB Context.
	 * 
	 * @author Ismail
	 *
	 */
	private static class JAXBContextLoader {
		private static final JAXBContext CACHED_CONTEXT;
		static {
			try {
				CACHED_CONTEXT = JAXBContext.newInstance(TXML.class);
				LOGGER.info("JAXB Context is Cached now");
			} catch (JAXBException e) {
				throw new ServiceException("Error while initialising the JAXB Context");
			}
		}
	}
}
