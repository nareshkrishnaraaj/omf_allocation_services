package com.order.splitter.util;

import java.math.BigDecimal;

/*
 * Represents the proration information for each type of amount for each lineitem
 */
public final class ProrationBean {

	private String orderLineId;
	private AmountType amountType;
	private String amountSubType;
	private BigDecimal quantity;
	private BigDecimal amountBeforeProration;
	private BigDecimal unitAmountAfterProration;
	private BigDecimal deltaAfterProration;
	private DiscountBean discountBean;
	
	/**
	 * @return the orderLineId
	 */
	public String getOrderLineId() {
		return orderLineId;
	}

	/**
	 * @param orderLineId the orderLineId to set
	 */
	public void setOrderLineId(String orderLineId) {
		this.orderLineId = orderLineId;
	}

	/**
	 * @return the amountType
	 */
	public AmountType getAmountType() {
		return amountType;
	}

	/**
	 * @param amountType the amountType to set
	 */
	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	/**
	 * @return the amountSubType
	 */
	public String getAmountSubType() {
		return amountSubType;
	}

	/**
	 * @param amountSubType the amountSubType to set
	 */
	public void setAmountSubType(String amountSubType) {
		this.amountSubType = amountSubType;
	}

	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the amountBeforeProration
	 */
	public BigDecimal getAmountBeforeProration() {
		return amountBeforeProration;
	}

	/**
	 * @param amountBeforeProration the amountBeforeProration to set
	 */
	public void setAmountBeforeProration(BigDecimal amountBeforeProration) {
		this.amountBeforeProration = amountBeforeProration;
	}

	/**
	 * @return the unitAmountAfterProration
	 */
	public BigDecimal getUnitAmountAfterProration() {
		return unitAmountAfterProration;
	}

	/**
	 * @param unitAmountAfterProration the unitAmountAfterProration to set
	 */
	public void setUnitAmountAfterProration(BigDecimal unitAmountAfterProration) {
		this.unitAmountAfterProration = unitAmountAfterProration;
	}

	/**
	 * @return the deltaAfterProration
	 */
	public BigDecimal getDeltaAfterProration() {
		return deltaAfterProration;
	}

	/**
	 * @param deltaAfterProration the deltaAfterProration to set
	 */
	public void setDeltaAfterProration(BigDecimal deltaAfterProration) {
		this.deltaAfterProration = deltaAfterProration;
	}

	/**
	 * @return the discountBean
	 */
	public DiscountBean getDiscountBean() {
		return discountBean;
	}

	/**
	 * @param discountBean the discountBean to set
	 */
	public void setDiscountBean(DiscountBean discountBean) {
		this.discountBean = discountBean;
	}

	private ProrationBean(Builder builder) {
		this.orderLineId=builder.orderLineId;
		this.amountType=builder.amountType;
		this.amountSubType=builder.amountSubType;
		this.quantity=builder.quantity;
		this.amountBeforeProration=builder.amountBeforeProration;
		this.discountBean = builder.discountBean;
	}
	
	public static class Builder {
		private String orderLineId;
		private AmountType amountType;
		private String amountSubType;
		private BigDecimal quantity;
		private BigDecimal amountBeforeProration;
		private BigDecimal unitAmountAfterProration;
		private BigDecimal deltaAfterProration;
		private DiscountBean discountBean;
		
		public Builder(String orderLineId, AmountType amountType, BigDecimal quantity) {
			this.orderLineId = orderLineId;
			this.amountType = amountType;
			this.quantity = quantity;
		}
		
		public Builder buildItemTotalBean(BigDecimal lineTotal) {
			this.amountBeforeProration=lineTotal;
			return this;
		}
		
		public Builder buildShippingChargeBean(BigDecimal shippingCharge) {
			this.amountBeforeProration=shippingCharge;
			return this;
			
		}
		
		public Builder buildHandlingChargeBean(BigDecimal handlingCharge) {
			this.amountBeforeProration=handlingCharge;
			return this;
		}
		
		public Builder buildDiscountTotalBean(String amountSubType, BigDecimal discountTotal, DiscountBean discountBean) {
			this.amountSubType=amountSubType;
			this.amountBeforeProration=discountTotal;
			this.discountBean = discountBean;
			return this;
		}
		
		public Builder buildShippingTaxBean(BigDecimal shippingTax, String subType) {
			this.amountBeforeProration = shippingTax;
			this.amountSubType=subType;
			return this;
		}
		
		public Builder buildSalesTaxBean(BigDecimal salesTax, String subType) {
			this.amountBeforeProration = salesTax;
			this.amountSubType=subType;
			return this;
		}
		
		public Builder buildHandlingTaxesBean(BigDecimal handlingTax, String subType) {
			this.amountBeforeProration = handlingTax;
			this.amountSubType=subType;
			return this;
		}
		
		public Builder buildMiscTaxesBean(BigDecimal miscTax, String subType) {
			this.amountBeforeProration = miscTax;
			this.amountSubType=subType;
			return this;
		}
		
		public Builder buildExtendedPurchasePriceBean(BigDecimal extPurchasePrice) {
			this.amountBeforeProration = extPurchasePrice;
			return this;
		}
		
		public Builder buildExtendedPriceBean(BigDecimal extPrice) {
			this.amountBeforeProration = extPrice;
			return this;
		}
		
		public ProrationBean build() {
			return new ProrationBean(this);
		}
	}
}
