package com.order.splitter.util;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.CustomFieldList.CustomField;

/*
 * Utitlity class for re-usable methods.
 */
public class Utility {
	
	private static final Logger LOGGER = LogManager.getLogger(Utility.class.getName());
	
	/**
	 * 
	 * @param orderLine
	 * @param attribute
	 * @return
	 */
	public  CustomField getCustomField(OrderLine orderLine, String attribute) {
		if(attribute != null) {
			List<CustomField> customFields = orderLine.getCustomFieldList().getCustomField();
			if(customFields != null) {
				for(CustomField customField: customFields) {
					if(customField != null && customField.getName() != null) {
						if(customField.getName().getValue().equalsIgnoreCase(attribute)) {
							LOGGER.debug("Attribute {} for orderLine# {} is present",attribute,orderLine.getLineNumber());
							return customField;
						}
					}
				}
			}
		}
		return null;
	}

}
