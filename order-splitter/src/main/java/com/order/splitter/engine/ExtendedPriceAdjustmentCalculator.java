package com.order.splitter.engine;

import java.math.RoundingMode;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.util.Constants;
import com.order.splitter.util.ProrationBean;

/*
 * Caclcuates the adjustments for Extender Purchase Price
 */
public class ExtendedPriceAdjustmentCalculator implements AdjustmentCalculator {

	private static final Logger LOGGER = LogManager.getLogger(ExtendedPriceAdjustmentCalculator.class.getName());
	
	/**
	 * Applies the proration for Extended Price
	 */
	@Override
	public List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean) {
		
		//Proration not required if the initial amount is 0.0
		if(proratedBean !=null && proratedBean.getAmountBeforeProration() !=null && 
				proratedBean.getAmountBeforeProration().compareTo(Constants.ZERO_VALUE)==0) {
			LOGGER.info("Amount before proration is 0.0. Skipping the proration");
			return orderLines;
		}
		
		boolean isDeltaCalculated = false;
		for(OrderLine orderLine:orderLines) {
			if(!isDeltaCalculated && proratedBean.getDeltaAfterProration().compareTo(Constants.ZERO_VALUE) == 1) {
				if(orderLine.getPriceInfo() != null && orderLine.getPriceInfo().getExtendedPrice() != null) {
					orderLine.getPriceInfo().getExtendedPrice().
					setValue(proratedBean.getUnitAmountAfterProration().add(proratedBean.getDeltaAfterProration()).setScale(2, RoundingMode.FLOOR).toPlainString());
				}
				isDeltaCalculated = true;
				LOGGER.debug("Extended Price with Delta {}",orderLine.getPriceInfo().getExtendedPrice().getValue());
			} else {
				if(orderLine.getPriceInfo() != null && orderLine.getPriceInfo().getExtendedPrice() != null) {
					orderLine.getPriceInfo().getExtendedPrice().
					setValue(proratedBean.getUnitAmountAfterProration().toPlainString());
				}
				if(!isDeltaCalculated) {
					isDeltaCalculated=true;
				}
				LOGGER.debug("Extended Price with No Delta {}",orderLine.getPriceInfo().getExtendedPrice().getValue());
			}
		}
		LOGGER.info("Proration Complete for the item");
		return orderLines;
	}

}
