package com.order.splitter.engine;

import java.math.RoundingMode;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.ChargeDetails.ChargeDetail;
import com.order.splitter.util.Constants;
import com.order.splitter.util.ProrationBean;

/*
 * Calculates adjustments for Shipping and Handling Fee, GiftBox
 */
public class ChargeDetailsAdjustmentCalculator implements AdjustmentCalculator {
	
	private static final Logger LOGGER = LogManager.getLogger(ChargeDetailsAdjustmentCalculator.class.getName());

	@Override
	public List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean) {
		boolean isDeltaCalculated = false;		
		//Proration not required if the initial amount is 0.0
		if(proratedBean !=null && proratedBean.getAmountBeforeProration() !=null && 
				proratedBean.getAmountBeforeProration().compareTo(Constants.ZERO_VALUE)==0) {
			LOGGER.info("Amount before proration is 0.0. Skipping the proration");
			return orderLines;
		}
		for(OrderLine orderLine:orderLines) {
			if(!isDeltaCalculated && proratedBean.getDeltaAfterProration().compareTo(Constants.ZERO_VALUE) == 1) {
				List<ChargeDetail> lineChargeDetails = orderLine.getChargeDetails().getChargeDetail();
				for(ChargeDetail charges:lineChargeDetails) {
					if(charges != null && charges.getChargeCategory().equalsIgnoreCase(proratedBean.getAmountType().getType())) {
						charges.setChargeAmount(proratedBean.getUnitAmountAfterProration().add(proratedBean.getDeltaAfterProration()).setScale(2, RoundingMode.FLOOR));
						LOGGER.debug("With Delta {} for  line# {} : {}",
								proratedBean.getAmountType().getType(),orderLine.getLineNumber(),charges.getChargeAmount());
					}
				}
				isDeltaCalculated = true;
			} else {
				List<ChargeDetail> lineChargeDetails = orderLine.getChargeDetails().getChargeDetail();
				for(ChargeDetail charges:lineChargeDetails) {
					if(charges != null && charges.getChargeCategory().equalsIgnoreCase(proratedBean.getAmountType().getType())) {
						charges.setChargeAmount(proratedBean.getUnitAmountAfterProration());
						LOGGER.debug("With No Delta {} for  line# {} : {}",
								proratedBean.getAmountType().getType(),orderLine.getLineNumber(),charges.getChargeAmount());
					}
				}
				if(!isDeltaCalculated) {
					isDeltaCalculated=true;
				}
			}
		}
		LOGGER.info("Shipping proration complete for "+proratedBean.getAmountType().getType());
		return orderLines;
	}

}
