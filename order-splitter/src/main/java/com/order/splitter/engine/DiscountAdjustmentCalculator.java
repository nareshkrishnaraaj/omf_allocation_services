package com.order.splitter.engine;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.DiscountDetails.DiscountDetail;
import com.order.splitter.util.Constants;
import com.order.splitter.util.ProrationBean;

/*
 * Prorates the Discount Amount on the orderlines based on the discount types - 
 * (Promotion, Coupon,Employee Discount, Shipping Disocunt) of the items
 */
public class DiscountAdjustmentCalculator implements AdjustmentCalculator {

	private static final Logger LOGGER = LogManager.getLogger(DiscountAdjustmentCalculator.class.getName());

	/**
	 * Applies the prorated discounts on the orderlines.
	 */
	@Override
	public List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean) {

		// Proration not required if the initial amount is 0.0
		if (proratedBean != null && proratedBean.getAmountBeforeProration() != null
				&& proratedBean.getAmountBeforeProration().compareTo(Constants.ZERO_VALUE) == 0) {
			LOGGER.info("Amount before proration is 0.0. Skipping the proration");
			return orderLines;
		}
		LOGGER.info("Applying Discount adjustments for orderLine# {}", proratedBean.getOrderLineId());
		LOGGER.debug("Proration Discount Type: {}", proratedBean.getDiscountBean().getDiscountType());
		boolean isDeltaCalculated = false;
		for (OrderLine orderLine : orderLines) {
			if (!isDeltaCalculated && proratedBean.getDeltaAfterProration().compareTo(Constants.ZERO_VALUE) == 1) {
				List<DiscountDetail> discountDetails = orderLine.getDiscountDetails().getDiscountDetail();
				for (DiscountDetail discount : discountDetails) {
					if (discount != null && discount.getDiscountAmount() != null) {
						boolean isDiscountApplied = applyDiscountsByType(discount, proratedBean,
								proratedBean.getDeltaAfterProration());
						if (isDiscountApplied)
							break;
					}
				}
				isDeltaCalculated = true;
			} else {

				List<DiscountDetail> discountDetails = orderLine.getDiscountDetails().getDiscountDetail();
				for (DiscountDetail discount : discountDetails) {
					if (discount != null && discount.getDiscountAmount() != null) {
						boolean isDiscountApplied = applyDiscountsByType(discount, proratedBean, Constants.ZERO_VALUE);
						if (isDiscountApplied)
							break;
					}
				}
				if (!isDeltaCalculated) {
					isDeltaCalculated = true;
				}
			}
		}
		LOGGER.info("Proration complete for OrderLine# {}, disocount type {} and SubType {}",
				proratedBean.getOrderLineId(), proratedBean.getAmountType(), proratedBean.getAmountSubType());
		return orderLines;
	}

	private boolean applyDiscountsByType(DiscountDetail discount, ProrationBean proratedBean, BigDecimal delta) {
		String beanDiscountType = proratedBean.getDiscountBean().getDiscountType();
		String beanDiscountId = proratedBean.getDiscountBean().getDiscountId();
		String itemDiscountDetailsType = discount.getDiscountType();
		String itemExtDiscountId = null;
		String itemCouponId = null;
		boolean isDiscountSet = false;

		if (discount != null) {
			if (discount.getCouponId() != null && discount.getCouponId().getValue() != null) {
				itemCouponId = discount.getCouponId().getValue();
			}
			itemExtDiscountId = (discount.getExtDiscountId() != null
					&& discount.getExtDiscountId().indexOf(Constants.HYPHEN) == -1)
							? discount.getExtDiscountId()
							: ((discount.getDiscountPercentage() != null
									&& discount.getDiscountPercentage().getValue() != null)
											? discount.getDiscountPercentage().getValue().toString()
											: (discount.getDescription() != null
													&& discount.getDescription().getValue() != null
															? (discount.getDescription().getValue())
															: Constants.BLANK));
		}

		LOGGER.debug("DiscountType: {}, DiscountId: {}, Item-DiscountType: {}, Item-CouponID: {}, Item-ExtDiscountId ",
				beanDiscountType, beanDiscountId, itemDiscountDetailsType, itemCouponId, itemExtDiscountId);

		if (itemDiscountDetailsType != null && beanDiscountType != null
				&& itemDiscountDetailsType.equalsIgnoreCase(beanDiscountType)) {
			if (!StringUtils.isEmpty(beanDiscountId)) {
				switch (beanDiscountType) {

				case Constants.COUPON:
					if ((itemCouponId != null && itemCouponId.equalsIgnoreCase(beanDiscountId))
							|| (itemExtDiscountId != null && itemExtDiscountId.equalsIgnoreCase(beanDiscountId))) {
						discount.setDiscountAmount(
								proratedBean.getUnitAmountAfterProration().add(delta).setScale(2, RoundingMode.FLOOR));
						isDiscountSet = true;
					}
					break;

				case Constants.PROMOTION:
				case Constants.BRD:
					if ((itemExtDiscountId != null && itemExtDiscountId.equalsIgnoreCase(beanDiscountId))
							|| (discount.getDiscountPercentage() != null
									&& discount.getDiscountPercentage().getValue() != null
									&& discount.getDiscountPercentage().getValue().toString()
											.equalsIgnoreCase(beanDiscountId))) {
						discount.setDiscountAmount(
								proratedBean.getUnitAmountAfterProration().add(delta).setScale(2, RoundingMode.FLOOR));
						isDiscountSet = true;
					}
					break;

				case Constants.EMPLOYEE:
					if (discount.getDiscountPercentage() != null && discount.getDiscountPercentage().getValue() != null
							&& discount.getDiscountPercentage().getValue().toString()
									.equalsIgnoreCase(beanDiscountId)) {
						// Since there can be multiple instances of Employee discount on the same
						// OrderLine with different discount values
						// there is a need to check, that amount before proration matches the correct
						// disocunt bean.
						if (discount.getDiscountAmount().compareTo(proratedBean.getAmountBeforeProration()) == 0) {
							discount.setDiscountAmount(proratedBean.getUnitAmountAfterProration().add(delta).setScale(2,
									RoundingMode.FLOOR));
							isDiscountSet = true;
						}
					}
					break;

				case Constants.APPEASEMENT:
					if ((itemExtDiscountId != null && itemExtDiscountId.equalsIgnoreCase(beanDiscountId))) {
						discount.setDiscountAmount(
								proratedBean.getUnitAmountAfterProration().add(delta).setScale(2, RoundingMode.FLOOR));
						isDiscountSet = true;
					}
					break;

				default:
					LOGGER.info("Discount Type is blank for item {} with discount-amount(before) {} ",
							proratedBean.getOrderLineId(), proratedBean.getAmountBeforeProration());
				}
			} else {
				/*
				 * LOGGER.info(
				 * "Emply discountBeanId for matched discountType. Applying proration to one of the discount tags of the same type."
				 * ); discount.setDiscountAmount(
				 * proratedBean.getUnitAmountAfterProration().add(delta).setScale(2,
				 * RoundingMode.FLOOR)); isDiscountSet = true;
				 */

				// If there are multiple Discounts of same type on an item which do not have a
				// discountId,
				// we check for the amounts to match before applyting the proaration

				if (discount.getDiscountAmount().compareTo(proratedBean.getAmountBeforeProration()) == 0) {
					discount.setDiscountAmount(
							proratedBean.getUnitAmountAfterProration().add(delta).setScale(2, RoundingMode.FLOOR));
					isDiscountSet = true;
				} else {
					LOGGER.info("Discount amount {} & amount before proration {} don't match",
							discount.getDiscountAmount(), proratedBean.getAmountBeforeProration());
				}

			}	
		}

		LOGGER.debug("Returning apply discounts with applied as {}", isDiscountSet);
		return isDiscountSet;
	}
}
