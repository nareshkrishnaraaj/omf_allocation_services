package com.order.splitter.engine;

import java.math.RoundingMode;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.CustomFieldList.CustomField;
import com.order.splitter.util.AmountType;
import com.order.splitter.util.Constants;
import com.order.splitter.util.ProrationBean;
import com.order.splitter.util.Utility;

/*
 * Calcuates the adjustments for Extended Purchase Price
 */
public class ExtendedPurchasePriceAdjustmentCalculator implements AdjustmentCalculator{

	private static final Logger LOGGER = LogManager.getLogger(ExtendedPurchasePriceAdjustmentCalculator.class.getName());
	
	@Override
	public List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean) {
		
		//Proration not required if the initial amount is 0.0
		if(proratedBean !=null && proratedBean.getAmountBeforeProration() !=null && 
				proratedBean.getAmountBeforeProration().compareTo(Constants.ZERO_VALUE)==0) {
			LOGGER.info("Amount before proration is 0.0. Skipping the proration");
			return orderLines;
		}
		boolean isDeltaCalculated = false;
		for(OrderLine orderLine:orderLines) {
			if(!isDeltaCalculated && proratedBean.getDeltaAfterProration().compareTo(Constants.ZERO_VALUE) == 1) {
				if(orderLine != null ) {
					CustomField customField = new Utility().getCustomField(orderLine, AmountType.EXT_PURCHASE_PRICE.getType());
					if(customField != null) {
						customField.getValue().
						setValue(proratedBean.getUnitAmountAfterProration().
								add(proratedBean.getDeltaAfterProration()).setScale(2, RoundingMode.FLOOR).toPlainString());
						LOGGER.debug("Extended PP with Delta {}",customField.getValue().getValue());
					}
				}
				isDeltaCalculated = true;
				
			} else {
				if(orderLine != null ) {
					CustomField customField = new Utility().getCustomField(orderLine, AmountType.EXT_PURCHASE_PRICE.getType());
					if(customField != null) {
						customField.getValue().
						setValue(proratedBean.getUnitAmountAfterProration().toPlainString());
						LOGGER.debug("Extended PP with No Delta {}",customField.getValue().getValue());
					}
				}
				if(!isDeltaCalculated) {
					isDeltaCalculated=true;
				}
			}
		}
		LOGGER.info("Proration Complete for the item");
		return orderLines;
	}

}
