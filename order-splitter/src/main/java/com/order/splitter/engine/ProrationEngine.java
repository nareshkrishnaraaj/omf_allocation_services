package com.order.splitter.engine;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.ChargeDetails.ChargeDetail;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.CustomFieldList.CustomField;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.DiscountDetails.DiscountDetail;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.TaxDetails.TaxDetail;
import com.order.splitter.util.AmountType;
import com.order.splitter.util.Constants;
import com.order.splitter.util.DiscountBean;
import com.order.splitter.util.ProrationBean;
import com.order.splitter.util.ProrationBean.Builder;

/*
 * OrderLine level proration and ajdustments are performed.
 */
@Service
public class ProrationEngine {

	private static final Logger LOGGER = LogManager.getLogger(ProrationEngine.class.getName());

	@Autowired
	AdjustmentCalculatorFactory factory;

	public List<OrderLine> executeProration(List<OrderLine> orderLines) {
		OrderLine orderLine = orderLines.get(0);

		// Initializes one prorationbean for each proration type
		List<ProrationBean> prorationBeans = buildProrationBeans(orderLine);

		// Updates the prorationbeans with the prorated amounts
		List<ProrationBean> proratedBeans = peformCalculcationAll(prorationBeans);

		// Apply the prorations on the orderLines
		applyProrations(orderLines, proratedBeans);

		return orderLines;
	}

	/**
	 * Perform prorations on the list of initial ProrationBeans and returns a List
	 * of adjusted ProrationBeans
	 * 
	 * @param beans
	 * @return
	 */
	private List<ProrationBean> peformCalculcationAll(List<ProrationBean> beans) {
		if (beans == null) {
			LOGGER.info("ProrationBeans is null");
			return Collections.emptyList();
		}
		List<ProrationBean> proratedBeans = new ArrayList<ProrationBean>(beans.size());
		LOGGER.info("ProrationBeans size {}", beans.size());
		if (beans != null && beans.size() > 0) {
			for (ProrationBean bean : beans) {
				if (bean != null) {
					proratedBeans.add(peformCalculcation(bean));
				}
			}
		}
		return proratedBeans;
	}

	/**
	 * Calculates the Adjustments based on the initial amount and the quantity
	 * 
	 * @param bean
	 * @return
	 */
	private ProrationBean peformCalculcation(ProrationBean bean) {
		BigDecimal quantity = bean.getQuantity();
		BigDecimal initialAmount = bean.getAmountBeforeProration();
		BigDecimal eachUnitAmount = initialAmount.divide(quantity, 2, RoundingMode.FLOOR);
		bean.setUnitAmountAfterProration(eachUnitAmount);
		BigDecimal deltaAmount = initialAmount
				.subtract(eachUnitAmount.multiply(quantity).setScale(2, RoundingMode.FLOOR));
		bean.setDeltaAfterProration(deltaAmount);
		LOGGER.debug(
				"OrderLine#::{}    AmountType::{}    SubType::{}    initialAmount::{}    ProratedUnitAmt::{}    DeltaAmount::{}",
				bean.getOrderLineId(), bean.getAmountType().toString(), bean.getAmountSubType(), initialAmount,
				eachUnitAmount, deltaAmount);
		return bean;
	}

	/**
	 * Applies the prorations to the OrderLines
	 * 
	 * @param orderLines
	 * @param proratedBeans
	 * @return
	 */
	private List<OrderLine> applyProrations(List<OrderLine> orderLines, List<ProrationBean> proratedBeans) {
		LOGGER.info("Applying prorations........");
		if (proratedBeans != null) {
			for (ProrationBean proratedBean : proratedBeans) {
				LOGGER.info("Discount amount type:::" + proratedBean.getAmountType());
				orderLines = applyAdjustments(orderLines, proratedBean);
			}
		}
		return orderLines;
	}

	/**
	 * Invokes the Adjustment Calcalators based on the Adjustment Type of the
	 * Proration bean
	 * 
	 * @param orderLines
	 * @param proratedBean
	 * @return
	 */
	private List<OrderLine> applyAdjustments(List<OrderLine> orderLines, ProrationBean proratedBean) {
		LOGGER.info("Initializing Adjustment calculators......");
		AdjustmentCalculator calculator = factory.createAdjustmentCalculator(proratedBean.getAmountType().getCode());
		List<OrderLine> updatedOrderLines = calculator.applyAdjustment(orderLines, proratedBean);
		return updatedOrderLines;
	}

	/**
	 * Initializes the ProrationBeans based on the given OrderLines
	 * 
	 * @param orderLine
	 * @return
	 */
	private List<ProrationBean> buildProrationBeans(OrderLine orderLine) {
		LOGGER.info("Initializing Prorationbeans for line#{}", orderLine.getLineNumber());
		List<ProrationBean> prorationBeans = new ArrayList<>();

		LOGGER.debug("Initializing Linetotal Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.LINETOTAL)
				.buildItemTotalBean(new BigDecimal(orderLine.getLineTotal().getValue())).build());

		LOGGER.debug("Initializing Shipping Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.SHIPPING)
				.buildShippingChargeBean(getShippingHandlingFee(orderLine, AmountType.SHIPPING.getType())).build());

		LOGGER.debug("Initializing Handling Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.HANDLING)
				.buildHandlingChargeBean(getShippingHandlingFee(orderLine, AmountType.HANDLING.getType())).build());

		LOGGER.debug("Initializing Misc (GiftBox) Prorationbean");
		// Gift Box charge details are expected to be present in Charge Details as
		// ChargeCategory = Misc
		prorationBeans.add(buildProrationBean(orderLine, AmountType.MISC_CHARGE)
				.buildHandlingChargeBean(getShippingHandlingFee(orderLine, AmountType.MISC_CHARGE.getType())).build());

		LOGGER.debug("Initializing SalesTax Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.SALES_TAX)
				.buildSalesTaxBean(getTaxByType(orderLine, AmountType.SALES_TAX.getType()), "Taxes").build());

		LOGGER.debug("Initializing ShippingTax Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.SHIPPING_TAX)
				.buildShippingTaxBean(getTaxByType(orderLine, AmountType.SHIPPING_TAX.getType()), "Taxes").build());

		LOGGER.debug("Initializing HandlingTax Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.HANDLING_TAX)
				.buildHandlingTaxesBean(getTaxByType(orderLine, AmountType.HANDLING_TAX.getType()), "Taxes").build());

		LOGGER.debug("Initializing MiscTax Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.MISC_TAX)
				.buildHandlingTaxesBean(getTaxByType(orderLine, AmountType.MISC_TAX.getType()), "Taxes").build());

		LOGGER.debug("Initializing ExtPurchasePrice Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.EXT_PURCHASE_PRICE)
				.buildExtendedPurchasePriceBean(getCustomFieldValue(orderLine, AmountType.EXT_PURCHASE_PRICE.getType()))
				.build());

		LOGGER.debug("Initializing ExtendedPrice Prorationbean");
		prorationBeans.add(buildProrationBean(orderLine, AmountType.EXT_PRICE)
				.buildExtendedPriceBean(new BigDecimal(orderLine.getPriceInfo().getExtendedPrice().getValue()))
				.build());

		prorationBeans.addAll(getDiscountProarationBeans(orderLine));
		LOGGER.debug("Initializing ExtendedPrice Prorationbean");

		return prorationBeans;
	}

	private Builder buildProrationBean(OrderLine orderLine, AmountType amountType) {
		return new ProrationBean.Builder(orderLine.getLineNumber(), amountType,
				orderLine.getQuantity().getOrderedQty());
	}

	/**
	 * Returns the Shipping or Handling fee for the OrderLine based on the type
	 * passed. Returns 0.0 if no charges is present.
	 * 
	 * @param orderLine
	 * @param type
	 * @return
	 */
	private BigDecimal getShippingHandlingFee(OrderLine orderLine, String type) {
		BigDecimal amountBeforeProration = new BigDecimal(0);
		if (orderLine != null && orderLine.getChargeDetails() != null
				&& orderLine.getChargeDetails().getChargeDetail() != null) {
			List<ChargeDetail> lineChargeDetails = orderLine.getChargeDetails().getChargeDetail();
			for (ChargeDetail charges : lineChargeDetails) {
				if (charges != null && charges.getChargeCategory() != null
						&& charges.getChargeCategory().equalsIgnoreCase(type)) {
					amountBeforeProration = charges.getChargeAmount();
				}
			}
		}
		LOGGER.debug("Before proration: {} fee --> {}", type, amountBeforeProration);
		return amountBeforeProration;
	}

	/**
	 * Returns Shipping, Sales, Handling Tax based on the type of Tax passed as the
	 * argument. Returns 0.0 if the requested tax type is not present.
	 * 
	 * @param orderLine
	 * @param type
	 * @return
	 */
	private BigDecimal getTaxByType(OrderLine orderLine, String type) {
		BigDecimal amountBeforeProration = new BigDecimal(0);
		if (orderLine != null && orderLine.getTaxDetails() != null
				&& orderLine.getTaxDetails().getTaxDetail() != null) {
			List<TaxDetail> taxDetails = orderLine.getTaxDetails().getTaxDetail();
			for (TaxDetail taxDetail : taxDetails) {
				if (taxDetail != null && taxDetail.getChargeCategory() != null
						&& taxDetail.getChargeCategory().getValue() != null
						&& taxDetail.getChargeCategory().getValue().equalsIgnoreCase(type)) {
					amountBeforeProration = taxDetail.getTaxAmount();
				}
			}
		}
		LOGGER.debug("Before proration: {} fee --> {}", type, amountBeforeProration);
		return amountBeforeProration;
	}

	/**
	 * Returns the Orderline customfield based on the attribute passed in the
	 * argument.
	 * 
	 * @param orderLine
	 * @param attribute
	 * @return
	 */
	private BigDecimal getCustomFieldValue(OrderLine orderLine, String attribute) {
		BigDecimal customAttrValue = new BigDecimal(0);
		if (attribute != null && orderLine.getCustomFieldList() != null
				&& orderLine.getCustomFieldList().getCustomField() != null) {
			List<CustomField> customFields = orderLine.getCustomFieldList().getCustomField();
			if (customFields != null) {
				for (CustomField customField : customFields) {
					if (customField != null && customField.getName() != null
							&& customField.getName().getValue() != null) {
						if (customField.getName().getValue().equalsIgnoreCase(attribute)) {
							try {
								if (customField.getValue() != null && customField.getValue().getValue() != null
										&& !customField.getValue().getValue().equals(Constants.BLANK)) {
									customAttrValue = new BigDecimal(customField.getValue().getValue());
								}
							} catch (ArithmeticException e) {
								LOGGER.error("Arithmetic Exception while parsing {}",
										customField.getValue().getValue());
							}
						}
					}
				}
			}
		}
		LOGGER.debug("Before proration: {} fee --> {}", attribute, customAttrValue);
		return customAttrValue;
	}

	/**
	 * Constructs the Collection of prorationbeans based on the type of discounts
	 * available on the orderLine. If there are no discounts, returns an empty
	 * Collection.
	 * 
	 * @param orderLine
	 * @return
	 */
	private List<ProrationBean> getDiscountProarationBeans(OrderLine orderLine) {
		List<ProrationBean> prorationBeans = new ArrayList<ProrationBean>();
		if (orderLine != null && orderLine.getDiscountDetails() != null
				&& orderLine.getDiscountDetails().getDiscountDetail() != null) {
			List<DiscountDetail> discountDetails = orderLine.getDiscountDetails().getDiscountDetail();
			if (discountDetails != null) {
				for (DiscountDetail discountDetail : discountDetails) {
					if (discountDetail != null) {

						/**
						 * Following DiscountTypes will be used to identify the unique Discounts using :
						 * DiscountBean:discountType & discountId. 1. If the DiscountType = Coupon,
						 * CouponId --> DiscountBean:discountId 2. If the DiscountType = BRD,
						 * ExternalDiscountId -->DiscountBean:discountId. ExternalDiscountId is the
						 * BrdID. 3. If the DiscountType = Promotion, ExternalDiscountId -->
						 * DiscountBean:DiscountId. ExternalDiscountId has a valid discountId if the
						 * promotion types are 'Shipping','Handling','GiftBox'. If the promotion-type is
						 * not one of the above mentioned types, DiscountPercentage -->
						 * DiscountBean:discountId, if present. else, Description -->
						 * DiscountBean:discountId, it could be blank as well. 4. If the DiscountType =
						 * Employee, DiscountPercentage will be used as DiscountBean:DiscountId.
						 *
						 * Above mapping will be used to assign the discounts back after proration.
						 */
						DiscountBean discountBean = new DiscountBean(discountDetail.getDiscountType(),
								getDiscountId(discountDetail));
						LOGGER.debug("Before proration: Discount --> {}", discountDetail.getDiscountAmount());
						prorationBeans.add(buildProrationBean(orderLine, AmountType.DISCOUNTAMOUNT)
								.buildDiscountTotalBean(AmountType.DISCOUNTAMOUNT.getType(),
										discountDetail.getDiscountAmount(), discountBean)
								.build());
					}
				}
			}
		}
		return prorationBeans;
	}

	/**
	 * Returns the discountId based on the type of Discount. Returns Empty if no
	 * discount type match is found.
	 * 
	 * @param discountDetail
	 * @return
	 */
	private String getDiscountId(DiscountDetail discountDetail) {

		if (discountDetail == null || discountDetail.getDiscountType() == null)
			return null;

		String discountType = discountDetail.getDiscountType();

		switch (discountType) {

		case Constants.COUPON:
			return (discountDetail.getCouponId() != null) ? discountDetail.getCouponId().getValue() : Constants.BLANK;

		case Constants.PROMOTION:
		case Constants.BRD:

			// For BRD, extDiscountId = Brd Number
			// For Promotions {Type = Shipping, Handling, GiftBox} extDiscountId =
			// predefined Id.
			// For rest of promotion types, return discount percentage if present, else the
			// description.
			return (discountDetail.getExtDiscountId() != null
					&& discountDetail.getExtDiscountId().indexOf(Constants.HYPHEN) == -1)
							? discountDetail.getExtDiscountId()
							: ((discountDetail.getDiscountPercentage() != null
									&& discountDetail.getDiscountPercentage().getValue() != null)
											? discountDetail.getDiscountPercentage().getValue().toString()
											: (discountDetail.getDescription() != null
													&& discountDetail.getDescription().getValue() != null
															? (discountDetail.getDescription().getValue())
															: Constants.BLANK));
		case Constants.EMPLOYEE:
			// return discount%, if present
			return (discountDetail.getDiscountPercentage() != null
					&& discountDetail.getDiscountPercentage().getValue() != null)
							? discountDetail.getDiscountPercentage().getValue().toString()
							: Constants.BLANK;

		// TODO: Validate this with an example. We do not have any sample of Appeasement
		// type.
		case Constants.APPEASEMENT:
			return (discountDetail.getExtDiscountId() != null
					&& discountDetail.getExtDiscountId().indexOf(Constants.HYPHEN) == -1)
							? discountDetail.getExtDiscountId()
							: "";

		default:
			return Constants.BLANK;
		}
	}
}
